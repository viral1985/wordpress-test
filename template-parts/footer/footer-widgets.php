<?php
/**
 * Displays the footer widget area
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since Twenty Nineteen 1.0
 */
?>
<div class="container">
	<div class="row">
	<?php
	if ( is_active_sidebar( 'sidebar-1' ) ) : ?>

		<div class="col-md-4">
			<?php
			if ( is_active_sidebar( 'footer-1' ) ) {
				?>
						<div class="widget-column footer-widget-1">
						<?php dynamic_sidebar( 'footer-1' ); ?>
						</div>
					<?php
			}
			?>
		</div><!-- .widget-area -->

	<?php endif; 
	if ( is_active_sidebar( 'footer-2' ) ) : ?>

		<div class="col-md-4">	
			<?php
			if ( is_active_sidebar( 'footer-2' ) ) {
				?>
						<div class="widget-column footer-widget-1">
						<?php dynamic_sidebar( 'footer-2' ); ?>
						</div>
					<?php
			}
			?>
		</div><!-- .widget-area -->

	<?php endif; 
	if ( is_active_sidebar( 'footer-3' ) ) : ?>

		<div class="col-md-4">	
			<?php
			if ( is_active_sidebar( 'footer-3' ) ) {
				?>
						<div class="widget-column footer-widget-1">
						<?php dynamic_sidebar( 'footer-3' ); ?>
						</div>
					<?php
			}
			?>
		</div><!-- .widget-area -->

	<?php endif; ?>
	</div>
</div>