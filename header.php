<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wordpress_Test
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'wordpress-test' ); ?></a>
   	<header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between wow fadeInDown" data-wow-duration="2s">
	 		<div class="container d-flex align-items-center">
	      <?php 
	        /* ----------------- Get Custom Logo Url and Display -------------- */
	        $site_logo_id = get_theme_mod( 'custom_logo' );
			$logo_image = wp_get_attachment_image_src( $site_logo_id , 'full' );
			if ( is_front_page() && is_home() ) :
				?>
				<!-- <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> -->
				<?php
			else :
				?>
				<!-- <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p> -->
				<?php
			endif;
			$wordpress_test_description = get_bloginfo( 'description', 'display' );
			if ( $wordpress_test_description || is_customize_preview() ) :
				?>
				<!-- <p class="site-description"><?php echo $wordpress_test_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p> -->
			<?php endif;
			if($logo_image){ ?>
				<div class="d-flex align-items-center col-4">
	          <a href="<?php echo site_url(); ?>" class="logo_link text-decoration-none">
		        <img src="<?php echo $logo_image[0]; ?>" class="img">
		      </a>
		    </div>
		    <div class="d-flex align-items-center col-8 justify-content-end">
		    	<div class="custom_menu d-flex flex-wrap justify-content-end">
		    		<button class="nav_btn"></button>
		    		<button class="nav_close_btn"></button>
			      <?php } 
			      /* ----------------- Get Menu and Display -------------- */
			        $primary_menu_items = wp_get_nav_menu_items('primary-menu');
			        if($primary_menu_items){
			           echo '
			           <ul class="nav justify-content-end">';
			           foreach ( $primary_menu_items as $navItem ) {
			               $target ='';
			                if($navItem->target){
			                	$target = "target=".$navItem->target;
			                }
			                /*$clasis = 'nav-link';
			                if(!empty($navItem->classes[0])){
			                	$clasis = implode($navItem->classes, ' ');
			                }else{
			                	$clasis = 'nav-link';
			                }*/
			         		echo '<li class="nav-item"><a href="'.$navItem->url.'" title="'.$navItem->title.'" class="nav-link" >'.$navItem->title.'</a></li>';
					   }
					   echo '</ul>';  
			        }
			      ?>
			    </div>
	      </div>
	    </div>
    </header>
  </div>
