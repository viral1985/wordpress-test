# Wordpress Test Document

=>Theme Installation Steps


     Step 1 : Login in to Wordpress Admin Panel
     Step 2 : Click on Appearance left side menu
     Step 3 : Click on Add New top button  : https://prnt.sc/13h8nc4
     Step 4 : Click on Upload Theme : https://prnt.sc/13h8ozr
     Step 5 : Select Wordpess Test theme Zip file and click on Install : https://prnt.sc/13h8t4y
     Step 6 : Please Wait till successfully install theme.

=> Below Steps to header settings
   
   # Logo Setting
   
      Step 1 : Click on Appearance->Customize : https://prnt.sc/13h91js
      Step 2 : Click on Site Identity tab : https://prnt.sc/13h937c
      Step 3 : Select Logo : https://prnt.sc/13h94hb
      Step 4 : Click on top Publish button to save.

   # Menu Setting
      Step 1 : Click on Appearance->Menu : https://prnt.sc/13h96h9
      Step 2 : Create New Menu and Add Menu items 
      Step 3 : Select Primary menu location
      Step 4 : Click on Save Menu to save menu.
      Please See Screenshot  : https://prnt.sc/13h99ec

      if you want to Button like this : https://prnt.sc/13h9dfv

      Please add menu item class "white_btn" : https://prnt.sc/13h9cev

   Finally Header Design Look like this : https://prnt.sc/13h9ec4

=> Below Steps to Footer settings
   
      =>You can add footer using below steps
         Step 1 : Click on Appearance->Widgets : https://prnt.sc/13h96h9
         Step 2 : Select footer widgets : https://prnt.sc/13ha7kg
   
   Finally Footer Look like this : https://prnt.sc/13ha8u9

=> Homapage Settings

   => Below Steps to manage Homepage

     Step 1 : Click on Pages->Add New left side menu : https://prnt.sc/13hab8l
     Step 2 : Enter Page Title
     Step 3 : Select "Homepage" Template from Template dropdown
     Step 4 : Click on Publish to save page
     Please see screenshot : https://prnt.sc/13haef1

   => Make Homepage as a frontend Page
   
     Step 1 : Click on Settings->Reading left side menu
     Step 2 : Select A static option
     Step 3 : Select Homapage
     Step 4 : Click on Save Changes to save.
      
