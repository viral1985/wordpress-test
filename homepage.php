<?php /* Template Name: Homepage */ 
get_header(); ?>
<div class="banner_section">
	<div class="video_overley">
		<video id="video_home" autoplay muted loop>
			<source src="<?php echo get_template_directory_uri(); ?>/assets/video/banner-video.mp4" type="video/mp4" />
		</video>
	</div>
	<div class="banner_txt">
		<div class="container">
		  <div class="row align-items-center">
		    <div class="col-md-6 wow fadeInLeft animated" data-wow-duration="2s">
		    	<h4>Duurzame huisjes</h4>
		    	<h2>Een ecologisch tiny huisje is zoveel mogelijk opgebouwd uit duurzame natuurlijke materialen</h2>
		    </div>
		    <div class="col-md-6 text-center wow fadeInRight animated" data-wow-duration="2s">
		    	<label class="icon-play" id="play_video">Bekijk de film</label>
		    </div>
		  </div>
		</div>
	</div>
  <div class="social_share">
  	<ul>
  		<li class="wow fadeInRight" data-wow-duration="0.75s" data-wow-delay="0.25s"><a href="https://www.instagram.com/sharer.php?u=<?php echo site_url();?>" target="_blank">Instagram</a></li>	
  		<li class="wow fadeInRight" data-wow-duration="0.75s" data-wow-delay="0.5s"><a href="https://www.facebook.com/sharer?u=<?php echo site_url();?>&t=<?php echo 'homepage'; ?>" target="_blank">Facebook</a></li>	
  		<li class="wow fadeInRight" data-wow-duration="0.75s" data-wow-delay="0.75s"><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo site_url();?>&title=<?php echo 'homepage'; ?>&summary=&source=<?php bloginfo('name'); ?>" target="_blank">Linkedin</a></li>	
  	</ul>
  </div>
</div>
<div class="dur_material sec_padding d-flex flex-wrap align-items-center">
	<div class="container">
		<div  class="row">
			<div class="col-md-6 order-2">
				<div class="about_img d-flex justify-content-end">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/image/LEVER-CLT.jpg" class="wow flipInY" data-wow-duration="1s" data-wow-delay="1s"/>
				</div>
			</div>
			<div class="col-md-6 order-1">
				<h3 class="sec_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.25s">Duurzame materialen</h3>
				<div class="text_content wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.5s">
					<p>Alle EcoCabins habeen een hoge isolatiewaarde, worden geproduceerd met indien mogelijk duurzame en/of natuurlijke materialen en hebben als opvallende uiterlijke kenmerk de 'ronde hoeken' mogelijk gemaakt door het specifieke materiaalgebruik. De voordelen van bouwen met hout zijn legio: hout is licht,flexibel,isoleert goed en bouwt snel en efficient.</p>
				</div>
				<a href="#" class="coman_btn wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">Button</a>
			</div>
		</div>
	</div>
</div>
<div class="innovate_sec theme_bg sec_padding">
	<div class="container">
		<div  class="row">
	    <h3 class="sec_title white_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.25s">Innovatie</h3>
	    <div class="col-md-6">
	    	<div class="text_content white_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.5s">
		    	<p>Wij hebben als doel om de EcoCabins voortdured vender te ontwikkelen en innoveren,zodat de EcoCabins uiteindelijk 100% duurzaan en circulair zijn. Voorwaarde hierbij is dat EcoCabins betaalbaar blijven. Met behulp van de zon, de wind en de beste isolatie geniet u van.</p>
		    </div>
	    </div>
	    <div class="col-md-6">
	    	<div class="text_content white_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.75s">
	    		<p>Alle EcoCabins habeen een hoge isolatiewaards, worden geproduceerd met indien mogelijk duurzame en/of natuurlijke materialen en habben , modelijk gemaakt door het specifieke materiaalgerbruik De voordelen van bouwen.</p>
	    	</div>
	    </div>
	  </div>
	</div>
</div>
<div class="how_it_work_sec sec_padding">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="how_img">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/image/house_img.jpg" class="wow flipInY" data-wow-duration="1s" />
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6 sub_col">
						<Div class="how_box">
							<h3 class="sec_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.20s">Hout</h3>
							<div class="text_content grey_txt wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.30s">
								<p>De draagconstructies van EcoCabins bestaan uit hout. Hout is duurzame en.</p>
							</div>
						</Div>
					</div>
					<div class="col-md-6 sub_col">
						<Div class="how_box">
							<h3 class="sec_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.50s">Glaswol isolatie</h3>
							<div class="text_content grey_txt wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.60s">
								<p>De combinatie van glaswol met een houtsketel constructies geeft een.</p>
							</div>
						</Div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 sub_col">
						<Div class="how_box">
							<h3 class="sec_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.80s">Afwerking </h3>
							<div class="text_content grey_txt wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="0.90s">
								<p>De draagconstructies van EcoCabins bestaan uit hout. Hout is duurzame en.</p>
							</div>
						</Div>
					</div>
					<div class="col-md-6 sub_col">
						<Div class="how_box">
							<h3 class="sec_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="1.10s">Triple glas</h3>
							<div class="text_content grey_txt wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="1.20s">
								<p>De combinatie van glaswol met een houtsketel constructies geeft een.</p>
							</div>
						</Div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 sub_col">
						<Div class="how_box">
							<h3 class="sec_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="1.40s">Stalen onderstel</h3>
							<div class="text_content grey_txt wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="1.50s">
								<p>De draagconstructies van EcoCabins bestaan uit hout. Hout is duurzame en.</p>
							</div>
						</Div>
					</div>
					<div class="col-md-6 sub_col">
						<Div class="how_box">
							<h3 class="sec_title wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="1.70s">Infrarood vloer</h3>
							<div class="text_content grey_txt wow fadeInUp" data-wow-duration="0.75s" data-wow-delay="1.80s">
								<p>De draagconstructies van EcoCabins bestaan uit hout. Hout is duurzame en.</p>
							</div>
						</Div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>