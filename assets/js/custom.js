/*********** Sidebar Js Start ***********/
$(document).ready(function() {
	$('.nav_btn').click(function(){
		$(this).closest('.custom_menu').addClass('menu_open');
	});
	$('.nav_close_btn').click(function(){
		$(this).closest('.custom_menu').removeClass('menu_open');
	});
	/**********************************/
	wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();
    document.getElementById('moar').onclick = function() {
      var section = document.createElement('section');
      section.className = 'section--purple wow fadeInDown';
      this.parentNode.insertBefore(section, this);
    };
});